from sys import argv
import os
from farm import plants
import farm.livestock as lvst


def adv_report():
    """
    Функция обработки аргументов для вывода краткого/расширенного отчёта
    и/или общего количества ресурсов за весь период "days".
    """
    global long_rep
    long_rep = False
    global rep_sum
    rep_sum = False
    global rep_to_file
    rep_to_file = False
    global days
    days = -1 # default
    for i, arg in enumerate(argv):
        if (len(argv[0]) > 1):
            if arg == '--days' or arg == '--d':
                try:
                    days = int(argv[i + 1])
                except IndexError:
                    input('Error during parsing argument parameter "days", recheck startup parameters.\n'
                          'Probably missed number.\n'
                          'Press ENTER to quit...')
                    os._exit(1)
                except ValueError:
                    input('Error during parsing argument parameter "days", recheck startup parameters.\n'
                          'Press ENTER to quit...')
                    os._exit(1)
            if arg == '--long' or arg == '--l':
                long_rep = True
            if arg == '--sum' or arg == '--s':
                rep_sum = True
            if arg == '--file' or arg == '--f':
                rep_to_file = True
    if days==-1:
        input('There is no time interval specified by the "--d" parameter.\n'
              'Check the startup parameters and/or refer to the documentation.\n'
              'Press ENTER to quit...')
        os._exit(1)


def economy(current_day, order, ec_d):
    """Составление отчета."""
    ec_d['bunnies'] = lvst.bunnies(current_day)
    if rep_sum == True:
        ec_d['corns'] += plants.corn(current_day)
        ec_d['milk'] += lvst.cow(current_day)
        ec_d['berries'] += plants.berries(current_day)
    else:
        ec_d['corns'] = plants.corn(current_day)
        ec_d['milk'] = lvst.cow(current_day)
        ec_d['berries'] = plants.berries(current_day)

    report = 'There are {} corns, {} bunnies, {} milk, {} berries '.format(ec_d['corns'], ec_d['bunnies'], ec_d['milk'],
                                                                           ec_d['berries'])
    return report


def filename():
    folder_name = 'reports'
    if not os.path.exists(folder_name):
        os.makedirs(folder_name)
    if long_rep and rep_sum:
        return folder_name + '/' + str(days) + '-days long report + sum_res'
    elif long_rep:
        return folder_name + '/' + str(days) + '-days long report'
    elif rep_sum:
        return folder_name + '/' + str(days) + '-days report + sum_res'
    else:
        return folder_name + '/' + str(days) + '-days report'


if __name__ == '__main__':
    """
    Модуль управления фермой.
    Выводит отчет о запасах на ферме в определенном интервале дней.
    """
    house_economy = {'corns': 0, 'bunnies': 0, 'milk': 0, 'berries': 0}
    adv_report()
    if rep_to_file:
        with open('{}.txt'.format(filename()), 'w') as text_report:
            if long_rep == True:
                for day in range(0, days + 1):
                    print("Day №", day, file=text_report)
                    print(economy(day, day % 30, house_economy), file=text_report)
                    print('---' * 10, file=text_report)
            else:
                for day in range(0, days + 1):
                    """
                    Накапливаем значения переменных "Кукуруза", "Молоко" и "Ягоды".
                    Каждый раз результаты сбрасываем в переменную "sum",
                    чтобы в последний день не вызывать функцию в "print()"
                    Костыль, но работает.
                    """
                    sum = economy(day, day % 30, house_economy)
                    if day == days:
                        print("Day №", day, file=text_report)
                        print(sum, file=text_report)
                        print('---' * 10, file=text_report)
        print('report saved to: ' + filename() + '.txt')
    else:
        if long_rep == True:
            for day in range(0, days + 1):
                print("Day №", day)
                print(economy(day, day % 30, house_economy))
                print('---' * 10)
        else:
            for day in range(0, days + 1):
                """
                Накапливаем значения переменных "Кукуруза", "Молоко" и "Ягоды".
                Каждый раз результаты сбрасываем в переменную "sum",
                чтобы в последний день не вызывать функцию в "print()"
                Костыль, но работает.
                """
                sum = economy(day, day % 30, house_economy)
                if day == days:
                    print("Day №", day)
                    print(sum)
                    print('---' * 10)

# input('PRESS ENTER TO EXIT')
