import random


def sowing(grains):
    """
    Случайное засевание поля.
    0 - ничего не выросло, 1 - урожайная ячейка.
    """
    field = []
    for _ in range(grains):
        i = random.randint(0, 1)
        field.append(i)
    return field  # return [random.randint(0, 1) for _ in range(grains)]


def corn(day):
    """Подсчёт початков кукурузы."""
    grains = (day + 1) * 2
    harvest = sowing(grains)
    counter = 0
    for corns in harvest:
        if corns == 1:
            counter += 1
    return counter


def berries(day):
    """
    Ягодный куст увеличивается в росте каждые 15 дней в 15 раз.
    Изначально с него можно собрать случайное количество ягод от 50 до 100.
    После прорастания этот диапазон увеличивается.
    """
    min = 50
    max = 100
    if (day // 15 >= 1):
        min *= (15 ** (day // 15))
        max *= (15 ** (day // 15))
    i = random.randint(min, max)
    return i
